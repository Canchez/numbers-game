// Функция, запускающая игру
function startGame() {
  if (document.querySelector("#nameInput").value != "") {
    document.querySelector("#timerContainer").style.display = "block";
    document.querySelector("#introBlock").style.display = "none";
    createTable();
    startTimer();
  }
}

let timerDisplay = document.querySelector(".timer");
let tInterval;

// Функция запуска секундомера
function startTimer() {
  let startTime = new Date().getTime();
  tInterval = setInterval(getShowTime, 10, startTime);
}

// Функция, реализующая секундомер
function getShowTime(startTime) {
  let updatedTime = new Date().getTime();
  let difference = updatedTime - startTime;
  let hours = Math.floor(
    (difference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
  );
  let minutes = Math.floor((difference % (1000 * 60 * 60)) / (1000 * 60));
  let seconds = Math.floor((difference % (1000 * 60)) / 1000);
  let hundredth = Math.floor((difference % 1000) / 10);
  hours = hours < 10 ? "0" + hours : hours;
  minutes = minutes < 10 ? "0" + minutes : minutes;
  seconds = seconds < 10 ? "0" + seconds : seconds;
  hundredth = hundredth < 10 ? "0" + hundredth : hundredth;
  timerDisplay.textContent =
    hours + ":" + minutes + ":" + seconds + ":" + hundredth;
}

// Функция остановки секундомера
function stopTimer(condition) {
  clearInterval(tInterval);
  if (condition === "win") {
    timerDisplay.style.background = "#00A90B";
    timerDisplay.style.color = "#0E6900";
  } else {
    timerDisplay.style.background = "#A90000";
    timerDisplay.style.color = "#690000";
  }
}

// Функция перезагрузки страницы
function rel() {
  location.reload();
}

let cellsArr;

// Функция создания таблицы
function createTable() {
  const fullTable = document.createElement("table");
  fullTable.id = "table";
  fullTable.cellSpacing = 0;
  for (let i = 0; i < 10; i++) {
    const tableRow = document.createElement("tr");
    fullTable.appendChild(tableRow);
    for (let j = 0; j < 10; j++) {
      const tableItem = document.createElement("td");
      tableItem.classList.add("cell");
      tableItem.id = `x${j}y${i}`;
      tableItem.onclick = function() {
        performStep(j, i, tableItem);
      };
      tableRow.appendChild(tableItem);
    }
  }
  document
    .querySelector("#timerContainer")
    .insertAdjacentElement("afterend", fullTable);
  cellsArr = document.querySelectorAll(".cell");
  cellsArr.forEach(item => (item.style.backgroundColor = "white"));
}

// Функция проверки ячейки на доступность
function checkCell(x, y) {
  let curElem = document.querySelector(`#x${x}y${y}`);
  if (x >= 0 && x <= 9 && y >= 0 && y <= 9 && curElem.textContent === "") {
    curElem.style.backgroundColor = "white";
    curElem.onclick = function() {
      performStep(x, y, this);
    };
    return 1;
  }
  return 0;
}

let filledCounter = 1;

// Функция, реализующая логику игры
function performStep(x, y, Element) {
  if (Element.textContent === "") {
    Element.textContent = filledCounter;
    let availableCounter = 0;
    if (filledCounter === 100) {
      stopTimer("win");
      document.querySelector("#res").textContent = "You won!";
      document.querySelector("#res").style.display = "block";
      document.querySelector("#but").style.display = "block";
      for (let cell of cellsArr) {
        cell.style.backgroundColor = "white";
        cell.onclick = "";
      }
    } else {
      filledCounter++;
      for (let cell of cellsArr) {
        cell.style.backgroundColor = "#20B2AA";
        cell.onclick = "";
      }
      Element.style.backgroundColor = "white";
      availableCounter += checkCell(x - 2, y - 1);
      availableCounter += checkCell(x - 2, y + 1);
      availableCounter += checkCell(x + 2, y - 1);
      availableCounter += checkCell(x + 2, y + 1);
      availableCounter += checkCell(x - 1, y - 2);
      availableCounter += checkCell(x - 1, y + 2);
      availableCounter += checkCell(x + 1, y - 2);
      availableCounter += checkCell(x + 1, y + 2);
      if (availableCounter === 0) {
        stopTimer("lose");
        document.querySelector("#res").textContent = "You lost!";
        document.querySelector("#res").style.display = "block";
        document.querySelector("#res").style.backgroundColor = "#A90000";
        document.querySelector("#res").style.color = "#330000";
        document.querySelector("#but").style.display = "block";
        Element.style.background = "#A90000";
      }
    }
  }
}
